"""
    Programme assistant vérifiant un dictionnaire ou calculant des fonctions mathématiques simples
    Code réalisé par Hadrien Allegaert et Leon Brandt
    
    file <name>: spécifie le nom d'un fichier sur lequel l'outil doit travailler
    info: montre le nombre de lignes et de caractères du fichier
    dictionary: utilise le fichier comme dictionnaire à partir de maintenant
    search <word>: cherche le mot le plus similaire au mot spécifié dans le dictionnaire
    sum <number1> ... <numbern>: calcule la somme des nombres spécifiés
    avg <number1> ... <numbern>: calcule la moyenne des nombres spécifiés
    help: montre des instructions à l utilisateur
    exit: arrête l outil
"""
import math
from random import shuffle
import difflib


def file(filename):
    """ Fonction servant à spécifié le fichier dans lequel on veut travailler.
    pre:'filename' est le paramètre du nom de fichier
    post: renvoi le fichier 'filename' ouvert en mode lecture
    """
    return open(filename, "r")

def info():
    """Compte le nombre de lignes et de caractères présent dans le fichier 'filename' ouvert grâce à la fonction 'file(filename)'.
    pre:
    post:
    """
    filein = file(files)
    lines = 0
    chars = 0
    for line in filein:
        lines+=1
        chars+= len(line)
    print("Le fichier possède " + str(lines) + " lignes")
    print("Le fichier possède " + str(chars) + " caractères")
    return
 

def dictionary():
    """Fonction définissant le fichier 'filename' ouvert dans la fonction 'file(filename)' comme dictionnaire.
    pre:
    post:
    """
    dico = {}
    filein = file(files)
    for line in filein:
        (key,val) = line.split(',')
        dico[key] = int(val)
    print(dico)
    return

def search():
    """ Fonction permettant la recherhce du mot le plus proche (le plus ressemblant) dans le dictionnaire.
        NON FONCTIONNELLE
    """
    proche = difflib.get_close_matches(command[1],files)
    print(proche)
    return 

def sum():
    """ Fonction calculant la somme des n nombres mis en parametres( command[1] à command[len(command)])
        Si le parametre n'est pas un 'int' ou un 'float' il sera automatiquement passé.
    pre:
    post:renvoi et imprime la somme contenue dans 'sum'
    """
    sum_f = 0
    for i in range(len(command)):
        try:
           if i >= 1:
               sum_f += int(command[i])
        except:
            print("Erreur : caractère '"+str(command[i])+ "' est invalide. Veuillez entrer des chiffres, ce paramètre sera donc passé.")
    print("La somme est égale à : " + str(sum_f))
    
    return sum_f

def avg():
    """ Calcule la moyenne des nombres choisis en paramètres (command[1] à command[len(command)])
        Si le parametre n'est pas un 'int' ou un 'float' il sera automatiquement passé.
    pre:
    
    post:imprime dans la console la moyenne des nombres choisis en paramètres contenue dans 'moy'
    """
    a = int(sum())
    moy = a / (len(command)-1)
    print("La moyenne des nombres choisis est :" + str(moy))

def help():
    """ Affiche la liste des commandes disponibles
    """
    print("file <name>: spécifie le nom d'un fichier sur lequel l'outil doit travailler\ninfo: montre le nombre de lignes et de caractères du fichier\ndictionary: utilise le fichier comme dictionnaire à partir de maintenant\nsearch <word>: cherche le mot le plus similaire au mot spécifié dans le dictionnaire\nsum <number1> ... <numbern>: calcule la somme des nombres spécifiés\navg <number1> ... <numbern>: calcule la moyenne des nombres spécifiés\nhelp: montre des instructions à l utilisateur\nexit: arrête l assistant\nqcm_start lance un mini qcm basique en console avec des réponses affichées en ordre aléatoire")
    return

def build_questionnaire(filename):
    """
        Construit le QCM avec les questions contenue dans le fichier donné.
        :type filename: Un string représentant le nom du fichier a charger.

        :return: Une liste de questions
    """
    questions = []
    wording = None
    choices = []
    with open(filename, encoding='utf-8') as file:
        for line in file.readlines():
            if '|' not in line:
                if wording or choices:
                    questions.append([wording, choices])
                wording = None
                choices = []
            else:
                parts = line.strip().split('|')
                if 1 < len(parts) < 5:
                    if parts[0] == 'Q':
                        if not wording and not choices:
                            wording = parts[1]
                            choices = []
                        else:
                            questions.append([wording, choices])
                            wording = None
                            choices = []
                    elif parts[0] == 'A':
                        if parts[2] not in ('V', 'X'):
                            print("Error when loading line:\n\t{}".format(line))
                        else:
                            choices.append([parts[1], parts[2] == 'V', parts[3] if len(parts) > 3 else ''])
                    else:
                        print("Error when loading line:\n\t{}".format(line))
                else:
                    print("Error when loading line:\n\t{}".format(line))

                if line.startswith('Q'):
                    wording = parts[1]

    if wording or choices:
        questions.append([wording, choices])
    return questions

def qcm_game():
    """Lance un qcm situé dans un fichier texte
    pre:
    post:
    """
    # Chargement du questionnaire
    questions = build_questionnaire("QCM.txt")
    new_ques1 = questions
    ans_l = []
    for q in range(len(questions)):
        print("\tQuestion " + str(q+1) + ": \"" + str(questions[q][0]) + "\"")
        """
            todo explication eth shuffle
        """
        shuffle(new_ques1[q][1])
        for r in range(len(questions[q][1])):
            print("\t\tReponses " + str(r+1) + ":" + new_ques1[q][1][r][0])
        answer = int(input("Entrez le chiffre correspondant à la réponse sélectionnée : "))
        ans_l.append(questions[q][1][answer-1][1])
    cote = 0
    for q in range(len(ans_l)):
        if ans_l[q]:
            cote +=1
    print("Votre score est de : "+ str(int(cote/len(ans_l)*10)), "/10")
    return

def closed_file():
    filebis = file(files)
    filebis.close()

"""
----------Execution-----------
"""

#user welcome
print("Bienvenue dans l'assistant H&L" + "\n" + "Si vous ne connaissez pas l'uitlisation de cet assistant veuillez taper 'help' afin de vous afficher la liste des commandes disponibles."+"\n"+"Veillez à séparer vos commandes par des espaces")
command = str(input("Veuillez entrer vos commandes"+"\n")).strip().split()
while command[0] != 'exit':
    if command[0] == 'help':
        help()
    elif command[0] == 'sum':
        sum()
    elif command[0] == 'file':
        file(command[1])
        files = command[1]
        print("Le fichier '" + str(command[1]) + "' est maintenant ouvert en mode lecture.")
        command = str(input("Veuillez entrer vos commandes" + "\n")).strip().split()       
        while command[0] != 'exit':
            if command[0] == 'dictionary':
                dictionary()                 
            elif command[0] == 'search':
                print('ok s/todo')
                search()                  
            elif command[0] == 'info':
                info()                  
            else:
                print("Pour continuer à utilisé le fichier texte veuillez utiliser les fonctions 'dictionary', 'info', ou 'search'"+"\n"+"Si vous voulez utiliser une des autres fonctions disponibles, veuillez utilisé 'exit'")
            command = str(input("Veuillez entrer vos commandes" + "\n")).strip().split()
        closed_file()
        command = str(input("Veuillez entrer vos commandes" + "\n")).strip().split()
        continue
    #débug si aucun fichier n'est ouvert
    elif command[0] == 'dictionary':
        print("Aucun fichier ouvert. Veuillez utilisé file")
    elif command[0] == 'search':
        print("Aucun fichier ouvert. Veuillez utilisé file")
    elif command[0] == 'info':
        print("Aucun fichier ouvert. Veuillez utilisé file")
    #appel de la fonction avg
    elif command[0] == 'avg':
        avg()
        
    elif command[0] == 'qcm_start':
        qcm_game()
    
    else:
        print("cette commande n'est pas reconnue")
    command = str(input("Veuillez entrer vos commandes"+"\n")).strip().split()